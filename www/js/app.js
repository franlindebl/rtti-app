// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//define variables
var openInAppBrowser = null;
var checkLocalStorage = null;
var ref = null;

angular.module('starter', ['ionic'])
.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		if(window.cordova && window.cordova.plugins.Keyboard) {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

			// Don't remove this line unless you know what you are doing. It stops the viewport
			// from snapping when text inputs are focused. Ionic handles this internally for
			// a much nicer keyboard experience.
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if(window.StatusBar) {
			// StatusBar.styleDefault();
			StatusBar.hide();
    		ionic.Platform.fullScreen();
		}

		openInAppBrowser = function(){
			// Open the URL using inappbrowser
			var url = "http://94.187.138.217:10080/KUWAIT-APP/#/map/GIS";
			var target = "_blank";
			var options = "location=no,zoom=no,toolbar=no";
			ref = window.cordova.InAppBrowser.open(url, target, options);

			ref.addEventListener( "loadstop", function() {
				// Clear out the urlToOpen in localStorage for subsequent opens.
				ref.executeScript({ code: "window.urlToOpen = null;" });

				// Add the event listener
				ref.executeScript({
					code: "window.addEventListener('message', function(event) { window.urlToOpen = event.data; }, false);"
				});

				// Start an interval
				setInterval(checkLocalStorage, 1000);
			});

			ref.addEventListener( "loaderror", function(event) {
				alert('error: ' + event.message);
				ref.close();
			});
		};

		checkLocalStorage = function() {
			// Execute JavaScript to check for the existence of a urlToOpen in the child browser's localStorage.
			ref.executeScript(
			{
				code: "window.urlToOpen"
			},
			function(values) {
				var urlToOpen = values[0];

				// If a urlToOpen was set, clear the interval and close the InAppBrowser.
				if (urlToOpen) {
					//Clear the local storage
					ref.executeScript({ code: "window.urlToOpen = null;" });

					// Open the system browser
					cordova.InAppBrowser.open(urlToOpen, "_system");
				}
			});
		};
		
		openInAppBrowser();
	});
})
